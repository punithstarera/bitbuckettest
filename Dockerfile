FROM ubuntu
MAINTAINER punithstarera@gmail.com
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RuN apt-get install -y apache2
RUN apt-get install -y apache2-utils
RUN apt-get clean
EXPOSE 80
CMD ["apache2ctl", "-D"," FOREGROUND"]

